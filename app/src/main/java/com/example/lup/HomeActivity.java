package com.example.lup;

import Commons.CustomPagerAdapter;
import Commons.DepthPageTransformer;
import Commons.ZoomOutPageTransformer;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

public class HomeActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;
    TextView pageTitle;
    View view;

    private int[] tabIconsId = {
            R.drawable.icon_white_home,
            R.drawable.icon_white_group,
            R.drawable.icon_white_appointment,
            R.drawable.icon_white_notification
    };

    private int[] selectedTabIconsId = {
            R.drawable.icon_dark_green_home,
            R.drawable.icon_dark_green_group,
            R.drawable.icon_dark_green_appointment_tab,
            R.drawable.icon_dark_green_notification
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.view_pager);
        //toolbar = findViewById(R.id.tool_bar);

        //setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.action_bar_custom);

        view = getSupportActionBar().getCustomView();
        pageTitle = view.findViewById(R.id.title_tv);

        CustomPagerAdapter pagerAdapter =
                new CustomPagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(new HomeFragment(), "Home");
        pagerAdapter.addFragment(new GroupFragment(), "Group");
        pagerAdapter.addFragment(new AppointmentFragment(), "Appointment");
        pagerAdapter.addFragment(new NotificationFragment(), "Notification");

        viewPager.setPageTransformer(true, new DepthPageTransformer());

        viewPager.setAdapter(pagerAdapter);

        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()){
                    case 0:
                        pageTitle.setText("Home");
                        tabLayout.getTabAt(0).setIcon(selectedTabIconsId[0]);
                    break;
                    case 1:
                        pageTitle.setText("Group");
                        tabLayout.getTabAt(1).setIcon(selectedTabIconsId[1]);
                    break;
                    case 2:
                        pageTitle.setText("Appointment");
                        tabLayout.getTabAt(2).setIcon(selectedTabIconsId[2]);
                    break;
                    case 3:
                        pageTitle.setText("Notification");
                        tabLayout.getTabAt(3).setIcon(selectedTabIconsId[3]);
                    break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tabLayout.getTabAt(0).setIcon(tabIconsId[0]);
                tabLayout.getTabAt(1).setIcon(tabIconsId[1]);
                tabLayout.getTabAt(2).setIcon(tabIconsId[2]);
                tabLayout.getTabAt(3).setIcon(tabIconsId[3]);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        for (int i = 0; i < tabIconsId.length; i++) {
            if(i != 0){
                tabLayout.getTabAt(i).setIcon(tabIconsId[i]);
            }
            tabLayout.getTabAt(0).setIcon(selectedTabIconsId[0]);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
}
