package com.example.lup;

import Commons.ProfileListCustomRecyclerViewAdapter;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class SelectedGroupActivity extends AppCompatActivity {
    String [] profileList = {"John Doe","Jane Doe"};
    RecyclerView profileListRv;
    ProfileListCustomRecyclerViewAdapter profileListCustomRecyclerViewAdapter;

    TextView pageTitle;
    View view;

    String selectedGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_group);

        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.action_bar_custom);

        view = getSupportActionBar().getCustomView();
        pageTitle = view.findViewById(R.id.title_tv);

        selectedGroup = getIntent().getStringExtra("group");
        pageTitle.setText(selectedGroup);

        profileListRv = findViewById(R.id.profile_list_rv);
        profileListRv.addItemDecoration(
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        profileListCustomRecyclerViewAdapter =
                new ProfileListCustomRecyclerViewAdapter(profileList, this);
        LinearLayoutManager verticalLayout = new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false);
        profileListRv.setLayoutManager(verticalLayout);
        profileListRv.setAdapter(profileListCustomRecyclerViewAdapter);
        profileListRv.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
                View childView = rv.findChildViewUnder(e.getX(), e.getY());

                if(childView != null){
                    int position = rv.getChildAdapterPosition(childView);
                    Toast.makeText(getApplicationContext(),
                            profileList[position], Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                    String [] groupInfo = {profileList[position], selectedGroup};
                    intent.putExtra("profile", groupInfo);
                    startActivity(intent);
                }
                return false;
            }

            @Override
            public void onTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
}
