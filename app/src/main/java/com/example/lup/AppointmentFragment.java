package com.example.lup;

import android.content.Intent;
import android.os.Bundle;

import Commons.AppointmentDialog;
import Commons.AppointmentListCustomRecyclerViewAdapter;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class AppointmentFragment extends Fragment {
    String [] appointmentList = {"John Doe","Jack Jill","Ronald Ken"};
    String [] appointmentStatus = {"pending","accepted","declined"};

    RecyclerView appointmentListRv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Enable menu
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_appointment, container, false);
        appointmentListRv = view.findViewById(R.id.appointment_list_rv);
        appointmentListRv.addItemDecoration(
                new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        AppointmentListCustomRecyclerViewAdapter appointmentListCustomRecyclerViewAdapter =
                new AppointmentListCustomRecyclerViewAdapter(
                        appointmentList, appointmentStatus, getActivity());
        LinearLayoutManager verticalLayout =
                new LinearLayoutManager(getActivity(),
                        LinearLayoutManager.VERTICAL, false);
        appointmentListRv.setLayoutManager(verticalLayout);
        appointmentListRv.setAdapter(appointmentListCustomRecyclerViewAdapter);
        appointmentListRv.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
                final View childView = rv.findChildViewUnder(e.getX(), e.getY());

                if(childView != null){
                    int position = rv.getChildAdapterPosition(childView);
                    AppointmentDialog appointmentDialog;

                    //Toast.makeText(getContext(), appointmentStatus[position], Toast.LENGTH_SHORT).show();


                    switch(appointmentStatus[position]){
                        case "accepted":
                                String [] appointmentAcceptedOptions = {
                                        "View profile",
                                        "Confirm appointment",
                                        "Change appointment",
                                        "Cancel appointment"
                                };
                                appointmentDialog = new AppointmentDialog(
                                        appointmentAcceptedOptions);
                                appointmentDialog.show(getFragmentManager(), "dialog");
                            break;
                        case "declined":
                                String [] appointmentDeclinedOptions = {
                                        "View profile",
                                        "Remove appointment"
                                };
                                appointmentDialog = new AppointmentDialog(
                                        appointmentDeclinedOptions);
                                appointmentDialog.show(getFragmentManager(), "dialog");
                            break;
                        default:
                                String [] appointmentPendingOptions = {
                                        "View profile",
                                        "Change appointment",
                                        "Cancel appointment"
                                };
                                appointmentDialog = new AppointmentDialog(
                                        appointmentPendingOptions);
                                appointmentDialog.show(getFragmentManager(), "dialog");
                            break;
                    }
                }
                return false;
            }

            @Override
            public void onTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_home, menu);
    }
}
