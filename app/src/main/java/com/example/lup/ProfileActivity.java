package com.example.lup;

import Commons.CommentListCustomRecyclerViewAdapter;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {
    TextView pageTitle;
    View view;
    FloatingActionButton setAppointmentFAB;

    Intent intent;

    String [] groupInfo;
    RecyclerView commentListRv;

    String[] commentList = {
            "John is a master of software development and agile.",
            "John helped my company design the right architecture for a problem we had."
    };

    CommentListCustomRecyclerViewAdapter commentListCustomRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.action_bar_custom);

        view = getSupportActionBar().getCustomView();
        pageTitle = view.findViewById(R.id.title_tv);

        groupInfo = getIntent().getStringArrayExtra("profile");
        pageTitle.setText(groupInfo[1]);

        commentListRv = findViewById(R.id.comments_list_rv);
        setAppointmentFAB = findViewById(R.id.fab);

        commentListRv.addItemDecoration(
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        commentListCustomRecyclerViewAdapter =
                new CommentListCustomRecyclerViewAdapter(commentList, this);
        LinearLayoutManager verticalLayout = new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false);
        commentListRv.setLayoutManager(verticalLayout);
        commentListRv.setAdapter(commentListCustomRecyclerViewAdapter);

        setAppointmentFAB.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id){
            case R.id.fab:
                intent = new Intent(
                        ProfileActivity.this, CreateAppointmentActivity.class);
                startActivity(intent);
            break;
        }
    }
}
