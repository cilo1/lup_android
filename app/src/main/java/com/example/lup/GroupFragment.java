package com.example.lup;

import android.content.Intent;
import android.os.Bundle;

import Commons.GroupListCustomRecyclerViewAdapter;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Layout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class GroupFragment extends Fragment {
    RecyclerView groupListRecyclerView;
    String [] groupList = {"Software_dev_80000","Software_dev_847500","Software_dev_64568"};

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set Fragment menu
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_group, container, false);

        groupListRecyclerView = view.findViewById(R.id.group_list_rv);

        groupListRecyclerView.addItemDecoration(
                new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        GroupListCustomRecyclerViewAdapter groupListCustomRecyclerViewAdapter =
                new GroupListCustomRecyclerViewAdapter(groupList, getActivity());
        LinearLayoutManager verticalLayout =
                new LinearLayoutManager(getActivity(),
                        LinearLayoutManager.VERTICAL, false);
        groupListRecyclerView.setLayoutManager(verticalLayout);
        groupListRecyclerView.setAdapter(groupListCustomRecyclerViewAdapter);
        groupListRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
                final View childView = rv.findChildViewUnder(e.getX(), e.getY());

                if(childView != null){
                    int position = rv.getChildAdapterPosition(childView);
                    Toast.makeText(getContext(), groupList[position], Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getContext(), SelectedGroupActivity.class);
                    intent.putExtra("group", groupList[position]);
                    startActivity(intent);
                }
                return false;
            }

            @Override
            public void onTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_home, menu);
    }
}
