package com.example.lup;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import Commons.GroupListCustomRecyclerViewAdapter;
import Commons.NotificationListCustomRecyclerViewAdapter;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;


public class NotificationFragment extends Fragment {
    String [] notificationList = {
            "Your request to John has been rejected",
            "A software developer has joined Software_dev_800023 group",
            "A business developer has joined Software_dev_800023 group"
    };

    RecyclerView notificationListRv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Enable menu
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        notificationListRv = view.findViewById(R.id.notification_list_rv);
        notificationListRv.addItemDecoration(
                new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        NotificationListCustomRecyclerViewAdapter notificationListCustomRecyclerViewAdapter =
                new NotificationListCustomRecyclerViewAdapter(notificationList, getActivity());
        LinearLayoutManager verticalLayout =
                new LinearLayoutManager(getActivity(),
                        LinearLayoutManager.VERTICAL, false);
        notificationListRv.setLayoutManager(verticalLayout);
        notificationListRv.setAdapter(notificationListCustomRecyclerViewAdapter);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_home, menu);
    }
}
