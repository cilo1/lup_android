package com.example.lup;

import Commons.DatePickerFragment;
import Commons.TimePickerFragment;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CreateAppointmentActivity extends AppCompatActivity
        implements View.OnClickListener {
    TextView pageTitle;
    View view;

    Button setDateBtn, setTimeBtn;
    TextView selectedDateTv, selectTimeTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_appointment);

        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.action_bar_custom);

        view = getSupportActionBar().getCustomView();
        pageTitle = view.findViewById(R.id.title_tv);

        setDateBtn = findViewById(R.id.set_date_btn);
        setTimeBtn = findViewById(R.id.set_time_btn);
        selectedDateTv = findViewById(R.id.selected_date_tv);
        selectTimeTv = findViewById(R.id.selected_time_tv);

        pageTitle.setText("Set an appointment");

        setDateBtn.setOnClickListener(this);
        setTimeBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch(id){
            case R.id.set_date_btn:
                    DatePickerFragment datePickerFragment = new DatePickerFragment(selectedDateTv);
                    datePickerFragment.show(getSupportFragmentManager(), "datePicker");
                break;
            case R.id.set_time_btn:
                    TimePickerFragment timePickerFragment = new TimePickerFragment(selectTimeTv);
                    timePickerFragment.show(getSupportFragmentManager(), "timePicker");
                break;
        }
    }
}
