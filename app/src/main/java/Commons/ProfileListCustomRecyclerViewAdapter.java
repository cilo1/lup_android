package Commons;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lup.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ProfileListCustomRecyclerViewAdapter extends RecyclerView.Adapter
        <ProfileListCustomRecyclerViewAdapter.ProfileListHolder> {
    String[] profileList;
    Context context;
    LayoutInflater layoutInflater;

    public ProfileListCustomRecyclerViewAdapter(String[] profileList, Context context) {
        this.profileList = profileList;
        this.context = context;
        layoutInflater = LayoutInflater.from(this.context);
    }

    @NonNull
    @Override
    public ProfileListCustomRecyclerViewAdapter.ProfileListHolder onCreateViewHolder(
            @NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(
                R.layout.custom_profile_list_recycler_item, parent, false);
        ProfileListHolder profileListHolder = new ProfileListHolder(view);
        return profileListHolder;
    }

    @Override
    public void onBindViewHolder(
            @NonNull ProfileListCustomRecyclerViewAdapter.ProfileListHolder holder,
            int position) {
        holder.nameTv.setText(profileList[position]);
    }

    @Override
    public int getItemCount() {
        return profileList.length;
    }

    public class ProfileListHolder extends RecyclerView.ViewHolder {
        TextView nameTv;

        public ProfileListHolder(@NonNull View itemView) {
            super(itemView);
            nameTv = itemView.findViewById(R.id.name_tv);
        }
    }
}
