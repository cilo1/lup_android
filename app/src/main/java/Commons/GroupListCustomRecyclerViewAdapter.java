package Commons;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lup.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class GroupListCustomRecyclerViewAdapter extends RecyclerView.Adapter
        <GroupListCustomRecyclerViewAdapter.GroupListHolder> {

    String [] groupList;
    Context context;
    LayoutInflater layoutInflater;

    public GroupListCustomRecyclerViewAdapter(String[] groupList, Context context) {
        this.groupList = groupList;
        this.context = context;
        layoutInflater = LayoutInflater.from(this.context);
    }

    @NonNull
    @Override
    public GroupListCustomRecyclerViewAdapter.GroupListHolder onCreateViewHolder(
            @NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.custom_group_list_recycler_item, parent, false);
        GroupListHolder groupListHolder = new GroupListHolder(view);
        return groupListHolder;
    }

    @Override
    public void onBindViewHolder(
            @NonNull GroupListCustomRecyclerViewAdapter.GroupListHolder holder, int position) {
        holder.groupListItemTv.setText(groupList[position]);
    }

    @Override
    public int getItemCount() {
        return groupList.length;
    }

    public class GroupListHolder extends RecyclerView.ViewHolder {
        TextView groupListItemTv;

        public GroupListHolder(@NonNull View itemView) {
            super(itemView);
            groupListItemTv = itemView.findViewById(R.id.group_item_name_tv);
        }
    }
}
