package Commons;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lup.R;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class AppointmentListCustomRecyclerViewAdapter extends RecyclerView.Adapter
        <AppointmentListCustomRecyclerViewAdapter.AppointmentListHolder> {

    String[] appointmentList, appointmentStatus;
    Context context;
    LayoutInflater layoutInflater;

    public AppointmentListCustomRecyclerViewAdapter(
            String[] appointmentList, String[] appointmentStatus, Context context) {
        this.appointmentList = appointmentList;
        this.appointmentStatus = appointmentStatus;
        this.context = context;
        layoutInflater = LayoutInflater.from(this.context);
    }

    @NonNull
    @Override
    public AppointmentListCustomRecyclerViewAdapter.AppointmentListHolder onCreateViewHolder(
            @NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(
                R.layout.custom_appointment_list_recycler_item, parent, false);
        AppointmentListHolder appointmentListHolder = new AppointmentListHolder(view);
        return appointmentListHolder;
    }

    @Override
    public void onBindViewHolder(
            @NonNull AppointmentListCustomRecyclerViewAdapter.AppointmentListHolder holder,
            int position) {
        holder.nameTv.setText(appointmentList[position]);

        switch(position){
            case 1:
                  holder.appointmentStatusIv.setImageResource(
                          R.drawable.icon_green_appointment_accept);
                  holder.appointmentStatusTv.setText("Accepted");
                  holder.appointmentStatusTv.setTextColor(
                          ContextCompat.getColor(this.context, R.color.colorAppointmentAccepted));
                break;
            case 2:
                holder.appointmentStatusIv.setImageResource(
                        R.drawable.icon_red_appointment_decline);
                holder.appointmentStatusTv.setText("Rejected");
                holder.appointmentStatusTv.setTextColor(
                        ContextCompat.getColor(this.context, R.color.colorAppointmentRejected));
                break;
            default:
                holder.appointmentStatusIv.setImageResource(
                        R.drawable.icon_orange_appointment_pending);
                holder.appointmentStatusTv.setText("Pending");
                holder.appointmentStatusTv.setTextColor(
                        ContextCompat.getColor(this.context, R.color.colorAppointmentPending));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return appointmentList.length;
    }

    public class AppointmentListHolder extends RecyclerView.ViewHolder {
        TextView nameTv, appointmentStatusTv;
        ImageView appointmentStatusIv;

        public AppointmentListHolder(@NonNull View itemView) {
            super(itemView);
            nameTv = itemView.findViewById(R.id.name_tv);
            appointmentStatusTv = itemView.findViewById(R.id.appointment_status_tv);
            appointmentStatusIv = itemView.findViewById(R.id.appointment_status_iv);
        }
    }
}
