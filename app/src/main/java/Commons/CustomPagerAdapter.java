package Commons;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

/**
 * Created by cilo on 5/28/18.
 */

public class CustomPagerAdapter extends FragmentStatePagerAdapter {
    int numOfTabs;
    String activityName;
    ArrayList<String> fragmentTitle;
    ArrayList<Fragment> fragmentArrayList;

    public CustomPagerAdapter(FragmentManager fm)  {
        super(fm);

        fragmentArrayList = new ArrayList<Fragment>();
        fragmentTitle = new ArrayList<String>();
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentArrayList.get(position);
    }

    public void addFragment(Fragment fragment, String title){
        fragmentArrayList.add(fragment);
        fragmentTitle.add(title);
    }

    @Override
    public int getCount() {
        return fragmentArrayList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        //return fragmentTitle.get(position);
        return null;
    }
}
