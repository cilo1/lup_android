package Commons;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lup.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CommentListCustomRecyclerViewAdapter extends RecyclerView.Adapter
        <CommentListCustomRecyclerViewAdapter.CommentListHolder> {
    String [] commentList;
    Context context;
    LayoutInflater layoutInflater;

    public CommentListCustomRecyclerViewAdapter(String[] commentList, Context context) {
        this.commentList = commentList;
        this.context = context;
        layoutInflater = LayoutInflater.from(this.context);
    }

    @NonNull
    @Override
    public CommentListCustomRecyclerViewAdapter.CommentListHolder onCreateViewHolder(
            @NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(
                R.layout.custom_comment_list_recycler_item, parent, false);
        CommentListHolder commentListHolder = new CommentListHolder(view);
        return commentListHolder;
    }

    @Override
    public void onBindViewHolder(
            @NonNull CommentListCustomRecyclerViewAdapter.CommentListHolder holder,
            int position) {
        holder.commentTv.setText(commentList[position]);
    }

    @Override
    public int getItemCount() {
        return commentList.length;
    }

    public class CommentListHolder extends RecyclerView.ViewHolder{
        TextView commentTv;

        public CommentListHolder(@NonNull View itemView) {
            super(itemView);
            commentTv = itemView.findViewById(R.id.comment_tv);
        }
    }
}
