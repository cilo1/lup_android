package Commons;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lup.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NotificationListCustomRecyclerViewAdapter extends RecyclerView.Adapter
        <NotificationListCustomRecyclerViewAdapter.NotificationListHolder> {

    Context context;
    String [] notificationList;
    LayoutInflater layoutInflater;

    public NotificationListCustomRecyclerViewAdapter(String[] notificationList, Context context) {
        this.context = context;
        this.notificationList = notificationList;
        layoutInflater = LayoutInflater.from(this.context);
    }

    @NonNull
    @Override
    public NotificationListCustomRecyclerViewAdapter.NotificationListHolder onCreateViewHolder(
            @NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(
                R.layout.custom_notification_list_recycler_item, parent, false);
        NotificationListHolder notificationListHolder = new NotificationListHolder(view);
        return notificationListHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationListCustomRecyclerViewAdapter.NotificationListHolder holder, int position) {
        holder.notificationItemTv.setText(notificationList[position]);
    }

    @Override
    public int getItemCount() {
        return notificationList.length;
    }

    public class NotificationListHolder extends RecyclerView.ViewHolder {
        TextView notificationItemTv;
        public NotificationListHolder(@NonNull View itemView) {
            super(itemView);
            notificationItemTv = itemView.findViewById(R.id.notification_item_tv);
        }
    }
}
